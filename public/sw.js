var CACHE_NAME = 'gantt'
var API_URL = "https://asymmetric-lore-201409.appspot.com/api/project"

self.addEventListener("activate", event => {
  const cacheWhitelist = [CACHE_NAME]
  event.waitUntil(
    caches.keys()
      .then(keyList =>
        Promise.all(keyList.map(key => {
          if (!cacheWhitelist.includes(key)) {
            return caches.delete(key)
          }
        }))
      )
  );
});

self.addEventListener("install", function(event) {
  event.waitUntil(
    caches.open(CACHE_NAME).then(function(cache) {
      fetch('asset-manifest.json').then(function(response) {
        response.json()
      }).then(function(assets) {
        const urlsToCache = [
          "/",
          "/index.html",
          assets['main.css'],
          assets['main.js']
        ]
        cache.addAll(urlsToCache)
      })
    })
  );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request).then(function(response) {
      if (response && event.request.url != API_URL) {
        return response
      }

      var fetchRequest = event.request.clone();

      return fetch(fetchRequest).then(
        function(response) {
          if(!response || response.status !== 200) {
            return response
          }

          var responseToCache = response.clone()

          caches.open(CACHE_NAME)
            .then(function(cache) {
              cache.put(event.request, responseToCache);
            })

          return response
        }
      );
    })
  );
});
