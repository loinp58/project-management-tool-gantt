import React, { Component } from 'react';
import Gantt from './components/Gantt';
import Toolbar from './components/Toolbar';
import MessageArea from './components/MessageArea';
import './App.css';
import { API_URL } from "./config"

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      links: [],
      currentZoom: 'Days',
      messages: []
    };
    this.handleZoomChange = this.handleZoomChange.bind(this);
    this.logTaskUpdate = this.logTaskUpdate.bind(this);
    this.logLinkUpdate = this.logLinkUpdate.bind(this);
  }


  componentWillMount() {
    let that = this;
    let networkDataReceived = false;
    fetch(`${API_URL}/project`).then(function (response) {
      response.json().then(function (tasks) {
        if (response.ok) {
          networkDataReceived = true;
          that.setState({ data: tasks.data, links: tasks.links });
        }
      })
    })

    if (process.env.NODE_ENV === "production") {
      caches.open('gantt').then(function(cache) {
        cache.match(`${API_URL}/project`).then(function (response) {
          if (response) {
            // throw Error("No data");
            return response.json()
          }
        }).then(function (data) {
          if (!networkDataReceived) {
            that.setState({ data: data.data, links: data.links });
          }
        })
      })
    }
  }

  addMessage(message) {
    let messages = this.state.messages.slice();
    let prevKey = messages.length ? messages[0].key : 0;

    messages.unshift({ key: prevKey + 1, message });
    if (messages.length > 40) {
      messages.pop();
    }
    this.setState({ messages });
  }

  logTaskUpdate(mode, task) {
    let text = task && task.text ? ` (${task.text})` : '';
    let message = `Task ${mode}: ${text}`;
    this.addMessage(message);
  }

  logLinkUpdate(id, mode, link) {
    let message = `Link ${mode}: ${id}`;
    if (link) {
      message += ` ( source: ${link.source}, target: ${link.target})`;
    }
    this.addMessage(message)
  }

  handleZoomChange(zoom) {
    this.setState({
      currentZoom: zoom
    });
  }

  render() {
    const { data, links } = this.state;
    let tasks = { data, links };
    return (
      <div>
        <Toolbar
          zoom={this.state.currentZoom}
          onZoomChange={this.handleZoomChange}
        />
        <div className="gantt-container">
          {data.length > 0 && <Gantt
            tasks={tasks}
            zoom={this.state.currentZoom}
            onTaskUpdated={this.logTaskUpdate}
            onLinkUpdated={this.logLinkUpdate}
          />}
        </div>
        <MessageArea
          messages={this.state.messages}
        />
      </div>
    );
  }
}
export default App;
