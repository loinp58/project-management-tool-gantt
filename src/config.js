export const APP_URL = process.env.NODE_ENV === 'production' ? 'https://asymmetric-lore-201409.appspot.com' : 'http://127.0.0.1:8000'
export const API_URL = `${APP_URL}/api`
