/*global gantt*/
import React, { Component } from 'react';
import 'dhtmlx-gantt';
import 'dhtmlx-gantt/codebase/dhtmlxgantt.css';
import { API_URL } from "../config";
import moment from 'moment';

export default class Gantt extends Component {
  setZoom(value) {
    switch (value) {
      case 'Hours':
        gantt.config.scale_unit = 'day';
        gantt.config.date_scale = '%d %M';

        gantt.config.scale_height = 60;
        gantt.config.min_column_width = 30;
        gantt.config.subscales = [
          { unit: 'hour', step: 1, date: '%H' }
        ];
        break;
      case 'Days':
        gantt.config.min_column_width = 70;
        gantt.config.scale_unit = "week";
        gantt.config.date_scale = "#%W";
        gantt.config.subscales = [
          { unit: "day", step: 1, date: "%d %M" }
        ];
        gantt.config.scale_height = 60;
        break;
      case 'Months':
        gantt.config.min_column_width = 70;
        gantt.config.scale_unit = "month";
        gantt.config.date_scale = "%F";
        gantt.config.scale_height = 60;
        gantt.config.subscales = [
          { unit: "week", step: 1, date: "#%W" }
        ];
        break;
      default:
        break;
    }
  }

  componentDidUpdate() {
    gantt.render();
  }

  shouldComponentUpdate(nextProps) {
    return this.props.zoom !== nextProps.zoom;
  }

  createDataTask(oldId, task) {
    var data = {
      text: task.text,
      type: task.type,
      start_date: moment(task.start_date).format('YYYY-MM-DD'),
      duration: task.duration,
      progress: 0,
      parent: task.parent
    }

    var url = API_URL + '/task/'
    fetch(url, {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(function (response) {
      response.json().then(function (result) {
        var newId = result.tid
        gantt.changeTaskId(oldId, newId)
      })
    })
  }

  updateDataTask(id, task) {
    var data = {
      text: task.text,
      type: task.type,
      start_date: moment(task.start_date).format('YYYY-MM-DD'),
      duration: task.duration,
      progress: task.progress,
      parent: task.parent
    }

    var url = API_URL + '/task/' + id
    fetch(url, {
      method: 'PUT',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(function (response) {
      if (response.ok && response.status === 200) {
        return true
      }
    })
  }

  deleteTask(id) {
    var url = API_URL + '/task/' + id
    fetch(url, {
      method: 'DELETE',
      headers: {
        'content-type': 'application/json'
      },
    }).then(function (response) {
      if (response.ok && response.status === 200) {
        return true
      }
    })
  }

  createLink(oldId, link) {
    var data = {
      source: link.source,
      target: link.target,
      type: link.type,
    }

    var url = API_URL + '/link'
    fetch(url, {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(function (response) {
      response.json().then(function (result) {
        var newId = result.tid
        gantt.changeLinkId(oldId, newId)
      })
    })
  }

  updateLink(id, link) {
    var data = {
      source: link.source,
      target: link.target,
      type: link.type,
    }

    var url = API_URL + '/link/' + id
    fetch(url, {
      method: 'PUT',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(function(response) {
      if (response.ok && response.status === 200) {
        return true
      }
    })
  }

  deleteLink(id) {
    var url = API_URL + '/link/' + id
    fetch(url, {
      method: 'DELETE',
      headers: {
        'content-type': 'application/json'
      }
    }).then(function(response) {
      if (response.ok && response.status === 200) {
        return true
      }
    })
  }

  initGanttEvents() {
    if (gantt.ganttEventsInitialized) {
      return;
    }
    gantt.ganttEventsInitialized = true;

    gantt.config.lightbox.sections = [
      { name: "description", height: 70, map_to: "text", type: "textarea", focus: true },
      { name: "type", type: "typeselect", map_to: "type" },
      { name: "time", type: "duration", map_to: "auto" }
    ];

    gantt.attachEvent('onAfterTaskAdd', (id, task) => {
      this.createDataTask(id, task);
      if (this.props.onTaskUpdated) {
        this.props.onTaskUpdated('inserted', task);
      }
    });

    gantt.attachEvent('onAfterTaskUpdate', (id, task) => {
      this.updateDataTask(id, task);
      if (this.props.onTaskUpdated) {
        this.props.onTaskUpdated('updated', task);
      }
    });

    gantt.attachEvent('onAfterTaskDelete', (id) => {
      this.deleteTask(id);
      if (this.props.onTaskUpdated) {
        this.props.onTaskUpdated(id, 'deleted');
      }
    });

    gantt.attachEvent('onAfterLinkAdd', (id, link) => {
      this.createLink(id, link);
      if (this.props.onLinkUpdated) {
        this.props.onLinkUpdated(link.id, 'inserted', link);
      }
    });

    gantt.attachEvent('onAfterLinkUpdate', (id, link) => {
      this.updateLink(id, link);
      if (this.props.onLinkUpdated) {
        this.props.onLinkUpdated(id, 'updated', link);
      }
    });

    gantt.attachEvent('onAfterLinkDelete', (id) => {
      this.deleteLink(id);
      if (this.props.onLinkUpdated) {
        this.props.onLinkUpdated(id, 'deleted');
      }
    });
  }

  componentDidMount() {
    this.initGanttEvents();
    gantt.init(this.ganttContainer);
    gantt.parse(this.props.tasks);
  }

  render() {
    if (this.props.zoom) {
      this.setZoom(this.props.zoom);
    }

    return (
      <div
        ref={(input) => { this.ganttContainer = input }}
        style={{ width: '100%', height: '100%' }}
      ></div>
    );
  }
}